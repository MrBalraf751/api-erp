const { client } = require('./dbConnection');

export async function getAllProducts() {
  const db = client.db(process.env.MONGODB_DB);
  const collection = db.collection('products');
  const products = await collection.find();
  return products;
}

export async function getProducts(req:any, res:any) {
  const productList = await getAllProducts();
  res.status(200).json(productList);
}
export async function getProductById(id: string) {
  const db = client.db(process.env.MONGODB_DB);
  const collection = db.collection('products');
  const product = await collection.findById(id);
  return product;
}
